package com.tvoykreditonlain;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.adjust.sdk.Adjust;
import com.adjust.sdk.AdjustAttribution;
import com.adjust.sdk.AdjustConfig;
import com.adjust.sdk.LogLevel;
import com.adjust.sdk.OnAttributionChangedListener;
import com.facebook.applinks.AppLinkData;
import com.onesignal.OneSignal;
import com.tvoykreditonlain.activities.MainActivity;


public class MainClass extends Application {

    //initializing variables
    public static String trackerToken, trackerName, network, campaign, adgroup, creative, adid, suid1, suid2, suid3, adgroup_id, creative_id, campaign_id;
    public static Float font;

    SharedPreferences settings;
    SharedPreferences.Editor editor;

    public static final String APPID = "com.tvoykreditonlain";
    public static final String REGION = "ua";
    private static final String ONESIGNAL_APP_ID = "3d5db181-4657-4985-a2c1-178163c4db99";

    //change variables fb ads
    public void fbAdsGetData() {
        //adgroup
        adgroup = MainActivity.adg.substring(0, MainActivity.adg.indexOf("(") - 1);
        adgroup_id = MainActivity.adg.substring(MainActivity.adg.indexOf("(") + 1, MainActivity.adg.indexOf(")"));

        //creative
        creative = MainActivity.cre.substring(0, MainActivity.cre.indexOf("(") - 1);
        creative_id = MainActivity.cre.substring(MainActivity.cre.indexOf("(") + 1, MainActivity.cre.indexOf(")"));

        //campaing
        campaign = MainActivity.cam.substring(0, MainActivity.cam.indexOf("(") - 1);
        campaign_id = MainActivity.cam.substring(MainActivity.cam.indexOf("(") + 1, MainActivity.cam.indexOf(")"));
    }

    //change variables google ads
    public void googleAdsGetData() {
        campaign = MainActivity.cam.substring(0, MainActivity.cam.indexOf("(") - 1);
        campaign_id = MainActivity.cam.substring(MainActivity.cam.indexOf("(") + 1, MainActivity.cam.indexOf(")"));

    }

    //fb attribution subid method
    private void getFBAtts(){
        //checking on first launch
        if (settings.contains("firstlaunch")){

        } else {
            //switch case
            switch (settings.getString("network", "")) {
                case "Facebook Installs":

                    break;

                case "Google Ads UAC":
                    editor.putString("sub1", "uac");
                    editor.putString("sub2", "uac");
                    editor.putString("sub3", "uac");
                    editor.commit();
                    break;

                case "Organic":
                    editor.putString("sub1", "organic");
                    editor.putString("sub2", "organic");
                    editor.putString("sub3", "organic");
                    editor.commit();
                    break;

                case "Unattributed":
                    editor.putString("sub1", "unattributed");
                    editor.putString("sub2", "unattributed");
                    editor.putString("sub3", "unattributed");
                    editor.commit();
                    break;

            }
        }

        //adding tag of first launch
        if(settings.contains("firstlaunch")){

        } else {
            editor.putInt("firstlaunch", 1);
            editor.commit();
        }
    }


    @Override
    public void onCreate() {
        super.onCreate();
        settings = getSharedPreferences("LOCAL", Context.MODE_PRIVATE);
        editor = settings.edit();
        // Configure adjust SDK.
        String appToken = "gxu9g3zadpfk";
        String environment = AdjustConfig.ENVIRONMENT_SANDBOX;
        AdjustConfig config = new AdjustConfig(this, appToken, environment);
        // enable all logs
        config.setLogLevel(LogLevel.VERBOSE);
        font = getResources().getConfiguration().fontScale;
        config.setOnAttributionChangedListener(new OnAttributionChangedListener() {
            @Override
            public void onAttributionChanged(AdjustAttribution attribution) {

                trackerToken = attribution.trackerToken;
                trackerName = attribution.trackerName;
                network = attribution.network;
                campaign = attribution.campaign;
                adgroup = attribution.adgroup;
                creative = attribution.creative;
                adid = attribution.adid;


                //put to sharedprefs
                editor.putString("trackerToken", trackerToken);
                editor.putString("trackerName", trackerName);
                editor.putString("network", network);
                editor.putString("campaign", campaign);
                editor.putString("adgroup", adgroup);
                editor.putString("creative", creative);
                editor.putString("adid", adid);
                editor.commit();
            }
        });
        Adjust.onCreate(config);

        // Enable verbose OneSignal logging to debug issues if needed.
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);

        // OneSignal Initialization
        OneSignal.initWithContext(this);
        OneSignal.setAppId(ONESIGNAL_APP_ID);

        //calling method to get fb attribution
        getFBAtts();



        registerActivityLifecycleCallbacks(new AdjustLifecycleCallbacks());
    }

    private static final class AdjustLifecycleCallbacks implements ActivityLifecycleCallbacks {
        @Override
        public void onActivityPreCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {

        }

        @Override
        public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {

        }

        @Override
        public void onActivityPostCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {

        }

        @Override
        public void onActivityPreStarted(@NonNull Activity activity) {

        }

        @Override
        public void onActivityStarted(@NonNull Activity activity) {

        }

        @Override
        public void onActivityPostStarted(@NonNull Activity activity) {

        }

        @Override
        public void onActivityPreResumed(@NonNull Activity activity) {

        }

        @Override
        public void onActivityResumed(Activity activity) {
            Adjust.onResume();
        }

        @Override
        public void onActivityPostResumed(@NonNull Activity activity) {

        }

        @Override
        public void onActivityPrePaused(@NonNull Activity activity) {

        }

        @Override
        public void onActivityPaused(Activity activity) {
            Adjust.onPause();
        }

        @Override
        public void onActivityPostPaused(@NonNull Activity activity) {

        }

        @Override
        public void onActivityPreStopped(@NonNull Activity activity) {

        }

        @Override
        public void onActivityStopped(@NonNull Activity activity) {

        }

        @Override
        public void onActivityPostStopped(@NonNull Activity activity) {

        }

        @Override
        public void onActivityPreSaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {

        }

        @Override
        public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {

        }

        @Override
        public void onActivityPostSaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {

        }

        @Override
        public void onActivityPreDestroyed(@NonNull Activity activity) {

        }

        @Override
        public void onActivityDestroyed(@NonNull Activity activity) {

        }

        @Override
        public void onActivityPostDestroyed(@NonNull Activity activity) {

        }

    }


}
