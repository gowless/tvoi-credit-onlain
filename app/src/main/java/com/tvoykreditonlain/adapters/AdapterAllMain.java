package com.tvoykreditonlain.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.adjust.sdk.Adjust;
import com.adjust.sdk.AdjustEvent;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.tvoykreditonlain.MainClass;
import com.tvoykreditonlain.R;
import com.tvoykreditonlain.activities.MainActivity;
import com.tvoykreditonlain.activities.SplashActivity;
import com.tvoykreditonlain.models.post.get.Liste;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

public class AdapterAllMain extends RecyclerView.Adapter<AdapterAllMain.ViewHolder> {
    public static String campaign, campaign_id, creative_id, creative, adgroup, adgroup_id, string;


    Context context;

    public void setDataList(List<Liste> dataList) {
        this.dataList = dataList;
        notifyDataSetChanged();
    }

    List<Liste> dataList;

    public AdapterAllMain(Context context, List<Liste> dataList) {
        this.context = context;
        this.dataList = dataList;

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view;
        LayoutInflater inflater = LayoutInflater.from(context);
        if (viewType == 1) {
            view = inflater.inflate(R.layout.fragment_beta_top, parent, false);
        } else {
            view = inflater.inflate(R.layout.fragment_beta, parent, false);
        }
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final Liste Liste = dataList.get(position);


        String firstCreditSum = Liste.getAmount().getFrom().toString();
        String percentRate = Liste.getPercent().getFrom().toString();

        //setting holders to textViews
        holder.firstCreditSum.setText(firstCreditSum + "₴");
        holder.percentRate.setText(percentRate + "%");
        holder.nextCreditSum.setText(dataList.get(position).getAmount().getTo().toString()+"₴");
        holder.payLoanTime.setText(dataList.get(position).getTerm().getFrom().toString() + "-" + dataList.get(position).getTerm().getTo().toString()+ " дней");


        //setting image holder with glide
        Glide.with(context)
                .load(dataList.get(position)
                        .getImg())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.progressBarGlide.setIndeterminate(false);
                        holder.progressBarGlide.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                       holder.progressBarGlide.setIndeterminate(false);
                       holder.progressBarGlide.setVisibility(View.GONE);
                        return false;
                    }
                })
                .centerInside()
                .into(holder.imgCompany);


        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //offer click event to adjust
                AdjustEvent adjustEvent = new AdjustEvent("h682o4");
                Adjust.trackEvent(adjustEvent);

                //starting default web-browser to current tab wit main URL
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(parseLinkFromApi(position)));
                v.getContext().startActivity(browserIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        //declaring items
        ConstraintLayout click_layout;
        TextView firstCreditSum, percentRate, payLoanTime, nextCreditSum;
        ImageView imgCompany;
        Button button;
        ProgressBar progressBarGlide;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            //initializing
            progressBarGlide = itemView.findViewById(R.id.progressBarGlide);
            button = itemView.findViewById(R.id.offerClick);
            imgCompany = itemView.findViewById(R.id.imgCompany);
            firstCreditSum = itemView.findViewById(R.id.firstCreditSum);
            percentRate = itemView.findViewById(R.id.percentRate);
            payLoanTime = itemView.findViewById(R.id.payLoanTime);
            nextCreditSum = itemView.findViewById(R.id.nextCreditSum);

        }
    }

    @Override
    public int getItemViewType(int position) {

        if (dataList.get(position).getTop() == null || dataList.get(position).getTop()) {
            return 1;
        } else {
            return 2;
        }
    }


    //getting current time for post request
    public String getCurrentTime() {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("HH-mm-ss-dd-MM-yyyy");
        Date date = new Date(System.currentTimeMillis());
        return formatter.format(date);
    }

    //getting android id
    public String getId() {
        @SuppressLint("HardwareIds")
        String android_id = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return android_id;
    }


    //change variables fb ads
    public void fbAdsGetData() {
        //adgroup
        adgroup = MainActivity.adg.substring(0, MainActivity.adg.indexOf("(") - 1);
        adgroup_id = MainActivity.adg.substring(MainActivity.adg.indexOf("(") + 1, MainActivity.adg.indexOf(")"));

        //creative
        creative = MainActivity.cre.substring(0, MainActivity.cre.indexOf("(") - 1);
        creative_id = MainActivity.cre.substring(MainActivity.cre.indexOf("(") + 1, MainActivity.cre.indexOf(")"));

        //campaing
        campaign = MainActivity.cam.substring(0, MainActivity.cam.indexOf("(") - 1);
        campaign_id = MainActivity.cam.substring(MainActivity.cam.indexOf("(") + 1, MainActivity.cam.indexOf(")"));

    }

    //change variables google ads
    public void googleAdsGetData() {
        campaign = MainActivity.cam.substring(0, MainActivity.cam.indexOf("(") - 1);
        campaign_id = MainActivity.cam.substring(MainActivity.cam.indexOf("(") + 1, MainActivity.cam.indexOf(")"));

    }


    public String parseLinkFromApi(int position) {
        // https://tds.pdl-profit.com?affid=18827&offer_id=1158&subid={client_id}&subid2={advertising_id}&subid3={app}&utm_source={source}&utm_campaign={campaign}&utm_adgroup={adgroup}&utm_adposition={adset}&utm_creative={chanel}"
        final Liste Liste = dataList.get(position);
        //Main URI declaring and initialising
        String mainEditedURI = Liste.getUrl();
        //manipulating with main string, changing parameters
        mainEditedURI = mainEditedURI.replaceAll(Pattern.quote("{client_id}"), MainActivity.subid1);
        mainEditedURI = mainEditedURI.replaceAll(Pattern.quote("{advertising_id}"), MainActivity.subid2);
        mainEditedURI = mainEditedURI.replaceAll(Pattern.quote("{app}"), MainActivity.subid3);

        //if organic/non-organic campaign
        if (MainActivity.net.equals("Organic")) {
            mainEditedURI = mainEditedURI.replaceAll(Pattern.quote("{source}"), "organic");
        } else if (MainActivity.net.equals("Unattributed")) {
            mainEditedURI = mainEditedURI.replaceAll(Pattern.quote("{source}"), "unattributed");
        } else {
            mainEditedURI = mainEditedURI.replaceAll(Pattern.quote("{source}"), MainActivity.net);
        }


        //if organic/non-organic campaign
        if (MainActivity.cam.equals("")) {
            mainEditedURI = mainEditedURI.replaceAll(Pattern.quote("{campaign}"), "organic");
        } else {
            mainEditedURI = mainEditedURI.replaceAll(Pattern.quote("{campaign}"), MainActivity.cam);

        }


        //if organic/non-organic adgroup
        if (MainActivity.adg.equals("")) {
            mainEditedURI = mainEditedURI.replaceAll(Pattern.quote("{adgroup}"), "organic");
        } else {
            mainEditedURI = mainEditedURI.replaceAll(Pattern.quote("{adgroup}"), MainActivity.adg);
        }


        //if organic/non-organic adset
        mainEditedURI = mainEditedURI.replaceAll(Pattern.quote("{adset}"), "organic");

        //if organic/non-organic chanel
        mainEditedURI = mainEditedURI.replaceAll(Pattern.quote("{chanel}"), "organic");

        //if organic/non-organic chanel
        mainEditedURI = mainEditedURI.replaceAll(Pattern.quote("{geo}"), "ua");

        Log.d("FINISH", mainEditedURI);
        return mainEditedURI;
    }

}