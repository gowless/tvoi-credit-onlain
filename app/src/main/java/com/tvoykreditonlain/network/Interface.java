package com.tvoykreditonlain.network;




import com.tvoykreditonlain.models.post.get.Data;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface Interface {

    //query for offers
    @GET("/{region}/{appid}")
    Call<Data> getData(
            @Path("region") String region,
            @Path("appid") String appId
    );




}
